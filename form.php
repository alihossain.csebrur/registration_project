<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Registration Form </title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-theme.min.css">
<link rel="stylesheet" href="css/style.css">

<script src="jquery/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script> 

<link rel="stylesheet" href="css/custom.css">
   
</head>

<body>
<div class="container">	
	<div class="row">
		<div class="header text-center">
			Registration Project
		</div>
	</div>
	
	<div class="row">
		<h1 class="text-center">Student Registration Form</h1><br><br>
		
		<form class="form-horizontal" action="form_process.php" method="POST">		
			<div class="form-group">
				<label class="control-label col-md-3">Student ID:</label>
				<div class="col-md-6">
					<input type="text" name="st_id" class="form-control" placeholder="Student ID">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3">Student Reg:</label>
				<div class="col-xs-6">
					<input type="text" name="st_reg" class="form-control" placeholder="Student Reg">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3">Department:</label>
				<div class="col-xs-6">
					<input type="text" name="dept" class="form-control"  placeholder="Department">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3">Faculty:</label>
				<div class="col-xs-6">
					<input type="text" name="faculty" class="form-control"  placeholder="Faculty">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3">Institution:</label>
				<div class="col-xs-6">
					<input type="text" name="inst" class="form-control"  placeholder="Faculty">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3">E-mail:</label>
				<div class="col-xs-6">
					<input type="email" name="email" class="form-control"  placeholder="E-mail">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3">Password:</label>
				<div class="col-xs-6">
					<input type="password" name="password" class="form-control" placeholder="Password">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3">Phone Number:</label>
				<div class="col-xs-6">
					<input type="text" name="phn_number" class="form-control" placeholder="Phone Number">
				</div>
			</div>
			
			
			<div class="form-group">
				<label class="control-label col-md-3" for="firstName">First Name:</label>
				<div class="col-xs-6">
					<input name="first_name" type="text" class="form-control" placeholder="First Name">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3">Last Name:</label>
				<div class="col-xs-6">
					<input name="last_name" type="text" class="form-control" id="lastName" placeholder="Last Name">
				</div>
			</div>
			
			
			<div class="form-group">
				<label class="control-label col-md-3">District:</label>
				<div class="col-xs-6">
					<input name="dist" type="text" class="form-control" placeholder="District">
				</div>
			</div>			
			
			<br>
			<div class="form-group">
				<div class="col-xs-offset-3 col-xs-6">
					<input type="submit" name="sub" class="btn btn-primary" value="Submit">
					<input type="reset" name="reset" class="btn btn-default" value="Reset">
				</div>
			</div>
		</form>
	</div>
</div>
</body>
</html>                                		